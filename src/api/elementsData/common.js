import request from '@/utils/request'

// 获取地区信息
export function regions(data) {
  return request({
    url: '/api/backstage/renew-custom/regions',
    method: 'get',
    params: data
  })
}
// 套餐类型
export function groupCenter(data) {
  return request({
    url: '/api/backstage/custom/group-center',
    method: 'get',
    params: data
  })
}
// 请求限制
export function requestList(data) {
  return request({
    url: '/api/backstage/custom/request-restriction-list',
    method: 'get',
    params: data
  })
}
// 数据级别
export function levelsList(data) {
  return request({
    url: '/api/backstage/custom/data-levels-list',
    method: 'get',
    params: data
  })
}
// 游戏列表
export function gameList(data) {
  return request({
    url: '/api/backstage/custom/games-list',
    method: 'get',
    params: data
  })
}
// 赛事状态
export function matchStatus(data) {
  return request({
    url: '/api/backstage/renew-custom/match-status',
    method: 'get',
    params: data
  })
}
