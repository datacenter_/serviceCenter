import request from '@/utils/request'

// 客户权限列表
export function custom_list(data) {
  return request({
    url: '/api/backstage/custom/custom-list',
    method: 'get',
    params: data
  })
}
// 客户详情
export function user_detail(data) {
  return request({
    url: '/api/backstage/custom/user-detail',
    method: 'get',
    params: data
  })
}
// 当前套餐数据
export function user_role(data) {
  return request({
    url: '/api/backstage/custom/user-role',
    method: 'get',
    params: data
  })
}
// 编辑套餐数据
export function user_role_edit(data) {
  return request({
    url: '/api/backstage/custom/user-role-edit',
    method: 'post',
    data
  })
}
// 历史套餐数据
export function user_role_history(data) {
  return request({
    url: '/api/backstage/custom/user-role-history',
    method: 'get',
    params: data
  })
}
// 默认套餐权限
export function default_role(data) {
  return request({
    url: '/api/backstage/custom/default-role',
    method: 'get',
    params: data
  })
}
// 当前游戏项目
export function user_game_role(data) {
  return request({
    url: '/api/backstage/custom/user-game-role',
    method: 'get',
    params: data
  })
}
// 历史游戏项目
export function user_game_role_history(data) {
  return request({
    url: '/api/backstage/custom/user-game-role-history',
    method: 'get',
    params: data
  })
}
// 私有赛事列表
export function tournaments_list(data) {
  return request({
    url: '/api/backstage/custom/user-private-tournaments-list',
    method: 'get',
    params: data
  })
}
//搜索私有赛事列表
export function tournaments_find(data) {
  return request({
    url: '/api/backstage/custom/tournaments-list',
    method: 'get',
    params: data
  })
}
//单独赛事详情
export function tournament_info(data) {
  return request({
    url: '/api/backstage/custom/tournament-info',
    method: 'get',
    params: data
  })
}
//新增编辑私有赛事
export function tournament_edit(data) {
  return request({
    url: '/api/backstage/custom/user-tournament-edit',
    method: 'post',
    data
  })
}
//删除私有赛事
export function tournament_deleted(data) {
  return request({
    url: '/api/backstage/custom/user-private-tournament-del',
    method: 'post',
    data
  })
}
//私有赛事详情
export function custom_tournament_info(data) {
  return request({
    url: '/api/backstage/custom/user-edit-tournament-info',
    method: 'get',
    params: data
  })
}
//用户游戏权限
export function user_game_role_edit(data) {
  return request({
    url: '/api/backstage/custom/user-game-role-edit',
    method: 'post',
    data
  })
}
//白名单
export function user_white_list(data) {
  return request({
    url: '/api/backstage/custom/user-white-list',
    method: 'get',
    params: data
  })
}
//添加编辑白名单
export function user_white_edit(data) {
  return request({
    url: '/api/backstage/custom/user-white-list-edit',
    method: 'post',
    data
  })
}
// 账号套餐列表
export function user_meal_list(data) {
  return request({
    url: 'api/backstage/renew-custom/user-set-meal-list',
    method: 'get',
    params: data
  })
}
// 账号套餐详情
export function user_meal_detail(data) {
  return request({
    url: 'api/backstage/renew-custom/user-set-meal-detail',
    method: 'get',
    params: data
  })
}
// 账号套餐编辑
export function user_meal_edit(data) {
  return request({
    url: 'api/backstage/renew-custom/user-set-meal-edit',
    method: 'post',
    data
  })
}
// 账号套餐删除
export function user_meal_del(data) {
  return request({
    url: 'api/backstage/renew-custom/user-set-meal-del',
    method: 'post',
    data
  })
}
// 账号游戏套餐列表
export function user_meal_game_list(data) {
  return request({
    url: 'api/backstage/renew-custom/user-set-meal-game-list',
    method: 'get',
    params: data
  })
}
//添加游戏套餐
export function user_game_edit(data) {
  return request({
    url: 'api/backstage/renew-custom/user-set-meal-game-edit',
    method: 'post',
    data
  })
}
//删除游戏套餐
export function user_game_del(data) {
  return request({
    url: 'api/backstage/renew-custom/user-set-meal-game-del',
    method: 'post',
    data
  })
}
// 账号套餐历史
export function user_meal_history(data) {
  return request({
    url: 'api/backstage/renew-custom/user-set-meal-history',
    method: 'get',
    params: data
  })
}
// 账号游戏历史
export function user_meal_game_history(data) {
  return request({
    url: 'api/backstage/renew-custom/user-set-meal-game-history',
    method: 'get',
    params: data
  })
}
// 用户操作日志
export function user_operation_log(data) {
  return request({
    url: 'api/backstage/renew-custom/user-operation-log',
    method: 'get',
    params: data
  })
}
// 私有赛事操作日志
export function user_tournaments_log(data) {
  return request({
    url: 'api/backstage/renew-custom/user-tournaments-log',
    method: 'get',
    params: data
  })
}
//修改密码
export function user_modify_password(data) {
  return request({
    url: 'api/backstage/renew-custom/business-modify-password',
    method: 'post',
    data
  })
}
