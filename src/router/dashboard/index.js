import Layout from '@/layouts/dashboardLayout'
import scopes_list from "../../views/gamecenter/scopes_list";
import api_usage_list from "../../views/gamecenter/api_usage_list";
import api_changelog_list from "../../views/gamecenter/api_changelog_list";
import center_list from "../../views/gamecenter/center_list";
import change_email_list from "../../views/gamecenter/change_email_list";
import change_password from "../../views/gamecenter/change_password";

const gamecenterRouters = [

  {
    path: '/dashboad',
    component: Layout,
    redirect: 'noRedirect',
    name: 'dashboad',
    meta: {title: 'KeyManagement', icon: 'users-cog'},
    // meta: {title: '客户权限管理', icon: 'users-cog', permissions: ['admin']},
    children: [
      // {
      //   path: 'kmc',
      //   name: 'kmc',
      //   component: () =>
      //     import('@/views/dashboard/kmc/index'),
      //   meta: {title: 'KeyManagement'},
      // },
      {
        path: '/gamecenter/scopes_list',
        name: 'scopes_list',
        component: scopes_list
      },
      {
        path: '/gamecenter/api_usage_list',
        name: 'api_usage_list',
        component: api_usage_list
      },
      {
        path: '/gamecenter/api_changelog_list',
        name: 'api_changelog_list',
        component: api_changelog_list
      },
      {
        path: '/gamecenter/center_list',
        name: 'center_list',
        component: center_list
      },
      {
        path: '/gamecenter/change_email_list',
        name: 'change_email_list',
        component: change_email_list
      },
      {
        path: '/gamecenter/change_password',
        name: 'change_password',
        component: change_password
      },
    ]
  },
]
export default gamecenterRouters
