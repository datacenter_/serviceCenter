import Layout from '@/layouts'

const gamecenterRouters = [
  {
    path: '/',
    component: Layout,
    redirect: '/index',
    children: [
      {
        path: '/index',
        name: 'Index',
        // component: () => import('@/views/index/index'),
        component: () => import('@/views/elementsdata/clientRules/index'),
        meta: {
          title: 'Home',
          icon: 'home',
          affix: true,
          noKeepAlive: true,
        },
      },
    ],
  },
  {
    path: '/elementsdata/clientRules',
    component: Layout,
    redirect: 'noRedirect',
    name: 'elementsdata',
    meta: {title: 'Rules', icon: 'users-cog'},
    hidden: true,
    // meta: {title: '客户权限管理', icon: 'users-cog', permissions: ['admin']},
    children: [
      {
        path: 'detail',
        name: 'detail',
        component: () =>
          import('@/views/elementsdata/clientRules/detail'),
        meta: {title: 'Detail'},
        hidden: true
      },
      {
        path: 'privateTournament',
        name: 'privateTournament',
        component: () =>
          import('@/views/elementsdata/clientRules/privateTournament'),
        meta: {title: 'Private Tournament'},
        hidden: true
      },
      {
        path: 'add',
        name: 'privateTournamentAdd',
        component: () =>
          import('@/views/elementsdata/clientRules/add'),
        meta: {title: 'Add Private Tournament Authority'},
        hidden: true
      },
      {
        path: 'edit',
        name: 'privateTournamentedit',
        component: () =>
          import('@/views/elementsdata/clientRules/edit'),
        meta: {title: 'Edit Private Tournament Authority'},
        hidden: true
      },
      {
        path: 'comopnents/tmpLog',
        name: 'tmpLog',
        component: () =>
          import('@/views/elementsdata/clientRules/comopnents/tmpLog'),
        meta: {title: 'Logs'},
        hidden: true
      },
    ]
  },
  {
    path: '/elementsdata/roles',
    component: Layout,
    redirect: 'noRedirect',
    name: 'roles',
    meta: {title: 'Rules', icon: 'users-cog'},
    hidden: true,
    // meta: {title: '客户权限管理', icon: 'users-cog', permissions: ['admin']},
    children: [
      {
        path: 'currentPlanTmp',
        name: 'planDetail',
        component: () =>
          import('@/views/elementsdata/roles/currentPlanTmp'),
        meta: {title: 'Edit Plan'},
        hidden: true
      },
      {
        path: 'add',
        name: 'add',
        component: () =>
          import('@/views/elementsdata/roles/add'),
        meta: {title: 'Add Plan'},
        hidden: true
      },
      {
        path: 'planHistory',
        name: 'planHistory',
        component: () =>
          import('@/views/elementsdata/roles/planHistory'),
        meta: {title: 'Plan History'},
        hidden: true
      },
    ]
  },
]
export default gamecenterRouters

