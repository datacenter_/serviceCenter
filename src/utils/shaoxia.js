const shaoxia = {
// JS取深度对象中的值
  safeProps(fn, defaultVal) {
    try {
      return fn();
    } catch (e) {
      return defaultVal;
    }
  },
  //js获取两个日期之间相差的天数
  getDaysBetween(dateString1,dateString2){
    if (dateString2){
      var  startDate = Date.parse(dateString1);
      var  endDate = Date.parse(dateString2);
      var days=(endDate - startDate)/(1*24*60*60*1000);
      // alert(days);
      return  days;
    }else {
      return 0
    }
  },
  // 指定日期是否是今天
  isToday(str){
    var d = new Date(str);
    if(d > new Date()){
      return true;
    } else {
      return false;
    }
  }
}
export default shaoxia

