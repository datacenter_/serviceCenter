FROM registry-intl.cn-hongkong.aliyuncs.com/bcdc/serice_center_base:1.0.0
COPY . /var/www/admin_front
RUN cd /var/www/admin_front ;\
npm run build
CMD ["/opt/start.sh"]
